#!/usr/bin/env python

from setuptools import setup, find_packages

with open('README.md', 'r') as f:
    long_description = f.read()

setup(name='rad',
    version='0.1',
    description='Rapid Application Development extensions',
    long_description=long_description,
    author='Ivan Sinkarenko',
    author_email='ivan.sinkarenko@cern.ch',
    url='https://wikis.cern.ch/display/ACCPY/PyQt+GUIs',
    packages=find_packages(exclude='example'),
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Environment :: X11 Applications :: Qt',
        'Operating System :: POSIX :: Linux',
    ],
    include_package_data=True,
    package_data={
        'rad.qt': 'rad/qt/*.ui',
        'rad.tools': 'rad/tools/*.ui',
    },
    install_requires = [
        'pyjapc',
        'pydm',
    ],
    entry_points={
        'gui_scripts': [
            'rad_designer=rad.launcher.main:designer',
            'rad_run=rad.launcher.main:pydm',
        ],
    },
    extras_require = {
        'test': [
            'pytest',
            'pytest-cov',
            'pytest-mock',
            'pytest-random-order',
        ],
    },
    test_suite='tests',
    )
