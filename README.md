# Install

## From source

```bash
git clone ...
cd rad
python3 -m venv .venv
ABSPATH=$(realpath .venv/lib/python*/site-packages)
echo "export PYTHONPATH=$ABSPATH:\$PYTHONPATH" >> .venv/bin/activate
source .venv/bin/activate
pip install --upgrade pip
pip install -e .
```

## From Gitlab

```bash
pip install git+https://gitlab.cern.ch/isinkare/rad.git@installation
```

After installation, it is advisable to setup environment variables in your `~/.bashrc`, so that
both Qt Designer and PyDM find plugins properly. Edit your `~/.bashrc` and add the following lines:
```bash
get_python_module_path() {
    python3 -c "import $1; import os; print(os.path.abspath(os.path.dirname$
}
export PYQTDESIGNERPATH="$(get_python_module_path rad.qt):$(get_python_module_path pydm_designer):$PYQTDESIGNERPATH"
export PYDM_DATA_PLUGINS_PATH="$(get_python_module_path rad.data):$PYDM_DATA_PLUGINS_PATH"
``` 

# Run
## To edit in Qt Designer

Assuming that your environment with new variables is sourced, launch `designer` command.
```bash
designer example/pydm/inspector_alt.ui
```

## To launch in PyDM environment

```bash
pydm example/pydm/inspector_alt.ui
```

## To launch the inspector

Open Inspector as described on Inspector wiki and open the file `example/inspector/inspector.xml` or run `inpector example/inspector/inspector.xml`.

# Test

Considering that you have installed the package from source, navigate to the root directory, and prepare test dependencies
```bash
pip install -e .[test]
```

Next, run tests:

```bash
python -m pytest
```

>
**Note!** If you want to retrieve the coverage information, currently pytest-cov may report incorrect coverage.
It is suggested to use `coverage` command directly to produce accurate results:

```bash
coverage run --source rad -m py.test && coverage report -m
```

or for HTML version: 
```bash
coverage run --source rad -m py.test && coverage html
```
>

If you have import errors, make sure that your `PYTHONPATH` is correct.
E.g. activating virtualenv does not set your `PYTHONPATH` automatically...

>
**Note!** Testing can be done in the randomized order to make sure that mocking does not affect adjacent tests. To run in random order,
```bash
python -m pytest --random-order
```