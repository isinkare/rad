import pydm
from os import path

class BeamPositioning(pydm.Display):

    def __init__(self, parent=None, args=None, macros=None):
        super(BeamPositioning, self).__init__(parent=parent, args=args, macros=macros)

    def ui_filename(self):
        # Point to our UI file
        return 'inspector_alt.ui'

    def ui_filepath(self):
        # Return the full path to the UI file
        return path.join(path.dirname(path.realpath(__file__)), self.ui_filename())
