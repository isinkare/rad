from PyQt5 import QtCore
from pydm.widgets import frame

class SyntheticDisplay(frame.PyDMFrame):

    synthetic_value_generated = QtCore.pyqtSignal(str)

    def __init__(self, parent, *args, **kwargs):
        super(SyntheticDisplay, self).__init__(parent=parent, *args, **kwargs)

    def value_changed(self, new_val: str):

        super(SyntheticDisplay, self).value_changed(new_val)

        if type(new_val) != 'str':
            self.log.warn('Only string values are supported for now')
            return

        self.synthetic_value_generated.emit(new_val + ' (decorated)')
