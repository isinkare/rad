import pydm
import types


class MyDisplay(pydm.Display):
    def __init__(self, parent=None, args=None, macros=None):
        super(MyDisplay, self).__init__(parent=parent, args=args, macros=macros)

        def value_changed_wrapper(self, new_value):
            self.setText(new_value + ' (decorated)')

        self.ui.MbedLabel.value_changed = types.MethodType(value_changed_wrapper, self.ui.MbedLabel)

    def ui_filename(self):
        return 'mbed.ui'