#!/usr/bin/python36

import sys
sys.path.append('/user/insp/deployments/libraries/python/PRO/')

from inspectorpy import InspectorCom

def avg(a, b):
    return (a + b) / 2

inspector = InspectorCom()
data = inspector.getInputVariable('$data')
result = data + ' (decorated)'
inspector.setOutputVariable('$output', result)
html = '<html><body><h2>Last value:</h2><hr /><p><strong>{}</strong>&nbsp;<span style="color: red">(decorated HTML)</span></p></body></html>'.format(data)
inspector.setOutputVariable('$html', html)

inspector.consolePrint('Calculated {} (HTML {})'.format(result, html))

inspector.end()
