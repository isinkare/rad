import os
from typing import Any, Dict


__all__ = [
    'designer',
    'pydm',
]


def _run_cmd(cmd: str, env: Dict[str, Any], **kwargs: Dict[str, Any]):
    import subprocess
    import sys
    sys_args = list(sys.argv)
    sys_args.pop(0)
    args = [cmd] + sys_args
    subprocess.run(args=args,
                   shell=False,
                   env=dict(os.environ, **env),
                   check=True,
                   **kwargs)


def designer():
    import rad.designer
    path_to_plugins = os.path.abspath(os.path.dirname(rad.designer.__file__))
    _run_cmd(cmd='designer', env=dict(PYQTDESIGNERPATH=path_to_plugins, QT_DESIGNER_RAD_EXTRAS='1'))


def pydm():
    import rad.data
    import rad.tools
    path_to_plugins = os.path.abspath(os.path.dirname(rad.data.__file__))
    path_to_tools = os.path.abspath(os.path.dirname(rad.tools.__file__))

    # The available environment variables are listed in the docs:
    # http://slaclab.github.io/pydm/configuration.html
    _run_cmd(cmd='pydm', env=dict(PYDM_DATA_PLUGINS_PATH=path_to_plugins,
                                  PYDM_DEFAULT_PROTOCOL='japc',
                                  PYDM_TOOLS_PATH=path_to_tools,
                                  ))
