import json
from pydm import tools
from typing import Optional


class DemoBaseTool(tools.ExternalTool):
    """
    Demo tool to try PyDM capabilities.

    Parameters
    ----------
    icon : QIcon
        The icon to be used when rendering the menu. Use None for no Icon.
    name : str
        The tool name. This must be unique across the combination group+name.
    group : str
        The group in which this action must be inserted. This becomes a
        QMenu with the tool as an action inside.
    author : str, optional
        This information is used to list the author name at the About screen.
    use_with_widgets : bool
        Whether or not this action should be rendered at a Custom Context Menu
        for the PyDMWidgets. If `False` the tool will be available at the Main Window
        menu only and will receive as a parameter `channels` as `None` and `sender` as
        the `main_window` object.

    """

    def __init__(self, name: str, use_with_widgets: bool, group: Optional[str] = None):
        super(DemoBaseTool, self).__init__(icon=None,
                                           name=name,
                                           group=group,
                                           author='CERN',
                                           use_with_widgets=use_with_widgets)

    def call(self, channels, sender):
        """
        This method is invoked when the tool is selected at the menu.

        Parameters
        ----------
        channels : list
            The list of channels in use at the widget.
        sender : PyDMWidget
            The PyDMWidget or Main Window that triggered the action.

        """
        class_name = self.__class__.__name__
        channels = '\n'.join(repr(x) for x in channels) if channels else 'None'
        sender = type(sender).__name__
        print(f'Called {class_name} with\nchannels:\n{channels}\n\nsender: {sender}')

    def to_json(self):
        """
        Serialize the information at this tool in order to make it possible
        to be added to another PyDM Application without user interference.

        Returns
        -------
        str
        """
        print(f'Serializing {self.__class__.__name__} info to JSON')
        return json.dumps({
            'global': not self.use_with_widgets,
        })

    def from_json(self, content):
        """
        Recreate the tool based on the serialized information sent as parameter.

        Parameters
        ----------
        content : str
        """
        print(f'Deserializing {self.__class__.__name__} info from JSON')
        dict = json.loads(content)
        is_global = dict['global']
        assert is_global != self.use_with_widgets
