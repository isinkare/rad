from rad.tools import tool_base
from os import path


class HiddenDemoTool(tool_base.DemoBaseTool):

    def __init__(self):
        super(HiddenDemoTool, self).__init__(name='Hidden Demo Tool',
                                             use_with_widgets=False)

    def get_info(self):
        """
        Retrieve basic information about the External Tool in a format
        that is parsed and used at the About screen.

        Returns
        -------
        dict
            Dictionary containing at least `author`, `file`, `group` and
            `name` of the External Tool.
        """
        orig = super(HiddenDemoTool, self).get_info()
        orig['file'] = path.realpath(__file__)
        return orig