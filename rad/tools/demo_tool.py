from rad.tools import tool_base
from os import path


# IN ORDER TO USE THESE FILES RIGHT AWAY, SET PYDM_TOOLS_PATH to the `rad` folder


class GlobalDemoTool(tool_base.DemoBaseTool):

    def __init__(self):
        super(GlobalDemoTool, self).__init__(name='Global Demo Tool',
                                             use_with_widgets=False)

    def get_info(self):
        """
        Retrieve basic information about the External Tool in a format
        that is parsed and used at the About screen.

        Returns
        -------
        dict
            Dictionary containing at least `author`, `file`, `group` and
            `name` of the External Tool.
        """
        orig = super(GlobalDemoTool, self).get_info()
        orig['file'] = path.realpath(__file__)
        return orig


class WidgetDemoTool(tool_base.DemoBaseTool):

    def __init__(self):
        super(WidgetDemoTool, self).__init__(name='Widget Demo Tool',
                                             group='Widgets demo',
                                             use_with_widgets=True)

    def get_info(self):
        """
        Retrieve basic information about the External Tool in a format
        that is parsed and used at the About screen.

        Returns
        -------
        dict
            Dictionary containing at least `author`, `file`, `group` and
            `name` of the External Tool.
        """
        orig = super(WidgetDemoTool, self).get_info()
        orig['file'] = path.realpath(__file__)
        return orig