from collections import namedtuple
import typing


DevicePropertyModel = namedtuple('DevicePropertyModel', 'device property field selector')


def parse_device_property_string(address: str) -> DevicePropertyModel:
    dev, path = address.split('/')
    prop, fieldsel = path.split('#')
    try:
        field, sel = fieldsel.split('@')
        return DevicePropertyModel(device=dev, property=prop, field=field, selector=sel)
    except ValueError:
        return DevicePropertyModel(device=dev, property=prop, field=fieldsel, selector=None)


def make_device_property_string(model: DevicePropertyModel) -> typing.Optional[str]:
    if not model.device or not model.property or not model.field:
        return None
    sel = '@' + model.selector if model.selector else ''
    return f'japc://{model.device}/{model.property}#{model.field}{sel}'