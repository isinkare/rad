import random
from pydm import widgets
from typing import Optional
from PyQt5 import QtGui, QtCore, QtWidgets


class CernRandomButton(widgets.PyDMPushButton):
    """ Button that generates a random number within given range. """

    def __init__(self,
                 parent: Optional[QtGui.QWindow] = None,
                 lower_limit: float = 0.0,
                 upper_limit: float = 1.0,
                 label: Optional[str] = None,
                 icon: Optional[str] = None,
                 init_channel: Optional[str] = None):
        super(CernRandomButton, self).__init__(parent=parent,
                                               label=label,
                                               icon=icon,
                                               init_channel=init_channel)
        self._upper_limit = upper_limit
        self._lower_limit = lower_limit

    def sendValue(self):
        """
        Overrides PyDM method to generate a random value instead of taking predefined one.

        Returns
        -------
        The value sent to the channel.
        """
        send_value = random.uniform(self._lower_limit, self._upper_limit)
        self.send_value_signal[self.channeltype].emit(send_value)
        return send_value

    @QtCore.pyqtProperty(float)
    def upperLimit(self) -> float:
        """ Upper limit for the random range. """
        return self._upper_limit

    @upperLimit.setter
    def upperLimit(self, value):
        """ Sets the upper limit value. """
        if self._upper_limit != value:
            self._upper_limit = value

    @QtCore.pyqtProperty(float)
    def lowerLimit(self) -> float:
        """ Lower limit for the random range. """
        return self._lower_limit

    @lowerLimit.setter
    def lowerLimit(self, value):
        """ Sets the lower limit value. """
        if self._lower_limit != value:
            self._lower_limit = value

    def textFn(self, text: str):
        print(f'Fake slot connected! {text}')


class CernMultiPageFrame(QtWidgets.QFrame):
    pass

class CernCustomSheetFrame(QtWidgets.QFrame):
    pass