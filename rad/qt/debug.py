import functools


def introspect(fn):
    @functools.wraps(fn)
    def wrapper(*args, **kwargs):
        try:
            obj = args[0]
            obj = obj.__class__.__name__ + '::'
        except IndexError:
            obj = ''
        argsstr = ', '.join(str(x) for x in args[1:])
        kwargsstr = ', ' + ', '.join(f'{k}={v}' for k, v in enumerate(kwargs)) if kwargs else ''
        print(f'--> {obj}{fn.__name__}({argsstr}{kwargsstr})', flush=True)
        res = fn(*args, **kwargs)
        print(f'<-- {obj}{fn.__name__}: {repr(res)}', flush=True)
        return res

    return wrapper