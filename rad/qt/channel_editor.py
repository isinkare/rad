from PyQt5 import QtWidgets, QtCore, uic, QtDesigner
from rad.qt import device
import os
import typing


__all__ = [
    'ChannelPropertyEditor',
    'ChannelCurveEditor',
]


class _BaseChannelEditor(QtWidgets.QDialog):

    items = {
        'isinkareDevice': {
            'Acquisition': [
                'bitEnum',
                'boolean',
                'booleanArray',
                'booleanArray2D',
                'double',
                'doubleArray',
                'doubleArray2D',
                'float',
                'floatArray',
                'floatArray2D',
                'int',
                'intArray',
                'intArray2D',
                'long',
                'longArray',
                'longArray2D',
                'short',
                'shortArray',
                'shortArray2D',
                'string',
                'stringArray',
                'stringArray2D',
            ],
            'Settings': [
                'bitEnum',
                'boolean',
                'booleanArray',
                'booleanArray2D',
                'double',
                'doubleArray',
                'doubleArray2D',
                'float',
                'floatArray',
                'floatArray2D',
                'int',
                'intArray',
                'intArray2D',
                'long',
                'longArray',
                'longArray2D',
                'short',
                'shortArray',
                'shortArray2D',
                'string',
                'stringArray',
                'stringArray2D',
            ],
            'highSpeedData': [
                'data1',
                'data2',
                'data3',
                'data4',
                'data5',
                'data6',
            ],
            'demoSettings': [
                'amplitude',
            ],
            'demoAcquisition': [
                'dataLeft',
                'dataRight',
                'statusLeft',
                'statusRight',
            ],
        },
    }

    def __init__(self, value: str, parent: QtCore.QObject = None):
        super(_BaseChannelEditor, self).__init__(parent)
        self.setup_ui()

        self._current_device = None
        self._current_prop = None

        try:
            self.value = device.parse_device_property_string(value)
        except (ValueError, AttributeError): # Saves from channel not being part of widget, or being of invalid format
            self.value = None

    def setup_ui(self):
        """ Loads UI file and binds signals/slots. """
        uic.loadUi(os.path.join(os.path.dirname(os.path.realpath(__file__)), 'selector.ui'), self)

        box: QtWidgets.QComboBox = self.field_combobox
        box.clear()
        box = self.prop_combobox
        box.clear()
        box.activated.connect(self.on_prop_selected)
        box = self.device_combobox
        box.clear()
        self._add_items_padded(combobox=box, items=self.items.keys())
        box.activated.connect(self.on_device_selected)

    def on_device_selected(self, index: int):
        box: QtWidgets.QComboBox = self.device_combobox
        new_device = box.itemText(index)
        if new_device != self._current_device:
            box = self.field_combobox
            box.clear()
            box = self.prop_combobox
            box.clear()
            try:
                new_props = self.items[new_device].keys()
            except KeyError:
                self._current_device = None
                self._current_prop = None
                return
            self._current_device = new_device
            self._add_items_padded(combobox=box, items=new_props)

    def on_prop_selected(self, index: int):
        box: QtWidgets.QComboBox = self.prop_combobox
        new_prop = box.itemText(index)
        if new_prop != self._current_prop:
            box = self.field_combobox
            box.clear()
            try:
                new_fields = self.items[self._current_device][new_prop]
            except KeyError:
                self._current_prop = None
                return

            self._current_prop = new_prop
            self._add_items_padded(combobox=box, items=new_fields)

    def _add_items_padded(self, combobox: QtWidgets.QComboBox, items: typing.List[str]):
        combobox.addItems([''] + list(items))


class ChannelPropertyEditor(_BaseChannelEditor):
    """ Dialog to select JAPC channel in Qt Designer. """

    def __init__(self, widget: QtWidgets.QWidget, parent: QtCore.QObject = None):
        super(ChannelPropertyEditor, self).__init__(value=widget.channel, parent=parent)
        self.widget = widget

    @QtCore.pyqtSlot()
    def save_changes(self):
        """ Slot for saving changes when the dialog is closed. """
        form_window = QtDesigner.QDesignerFormWindowInterface.findFormWindow(self.widget)
        if form_window:
            model = device.DevicePropertyModel(device=self.device_combobox.currentText(),
                                               property=self.prop_combobox.currentText(),
                                               field=self.field_combobox.currentText(),
                                               selector=self.selector_widget.selector)
            form_window.cursor().setProperty('channel', device.make_device_property_string(model))

    def setup_ui(self):
        """ Loads UI file and binds signals/slots. """
        super(ChannelPropertyEditor, self).setup_ui()
        self.button_box.accepted.connect(self.save_changes)


class ChannelCurveEditor(_BaseChannelEditor):
    """ Dialog to select JAPC channel in Qt Designer. """

    def __init__(self, value: str, parent: QtCore.QObject = None):
        super(ChannelCurveEditor, self).__init__(value=value, parent=parent)
        self.resulting_val = None

    @QtCore.pyqtSlot()
    def save_changes(self):
        """ Slot for saving changes when the dialog is closed. """
        model = device.DevicePropertyModel(device=self.device_combobox.currentText(),
                                           property=self.prop_combobox.currentText(),
                                           field=self.field_combobox.currentText(),
                                           selector=self.selector_widget.selector)
        self.resulting_val = device.make_device_property_string(model)

    def setup_ui(self):
        """ Loads UI file and binds signals/slots. """
        super(ChannelCurveEditor, self).setup_ui()
        self.button_box.accepted.connect(self.save_changes)
