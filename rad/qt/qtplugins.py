import logging
logger = logging.getLogger(__name__)

import typing
import threading
import re
from pydm.widgets.qtplugin_base import qtplugin_factory
from pydm.widgets import qtplugin_extensions
from PyQt5 import QtGui, QtDesigner, QtCore, QtWidgets
from . import widgets as cern_widgets, channel_editor
from .debug import introspect
# import PyCernQt
# import CyQtDesigner


__all__ = [
    'CernRandomButtonPlugin',
    'CernMultiPageFramePlugin',
    'CernCustomFormModificationPlugin',
]

_BASE_EXTENSIONS = [qtplugin_extensions.RulesExtension]


_PROPERTY_SHEET_EXTENSION_IID = 'org.qt-project.Qt.Designer.PropertySheet'
_MEMBER_SHEET_EXTENSION_IID = 'org.qt-project.Qt.Designer.MemberSheet'
_TASK_MENU_EXTENSION_IID = 'org.qt-project.Qt.Designer.TaskMenu'
_CONTAINER_EXTENSION_IID = 'org.qt-project.Qt.Designer.Container'


_LOGO_XPM = [
    "16 16 4 1",
    " 	c None",
    ".  c #ffff00",
    "a	c #000000",
    "-	c #b2b2b2",
    '    --aaaa--    ',
    '    --aaaa--    ',
    '  a-..-..-..-a  ',
    '  a-..-..-..-a  ',
    ' a...aa..aa...a ',
    ' a...aa..aa...a ',
    'a..............a',
    'a..............a',
    'a..aa-....-aa..a',
    'a..aa-....-aa..a',
    ' a..aaa--aaa..a ',
    ' a..aaa--aaa..a ',
    '  a-..-aa-..-a  ',
    '  a-..-aa-..-a  ',
    '    --aaaa--    ',
    '    --aaaa--    ',
]


class JapcSelectorExtension(qtplugin_extensions.PyDMExtension):

    def __init__(self, widget):
        super(JapcSelectorExtension, self).__init__(widget)
        self.widget = widget
        self.edit_channel_action = QtWidgets.QAction("Edit Channel...", self.widget)
        self.edit_channel_action.triggered.connect(self.edit_channel)

    def edit_channel(self, state):
        del state
        edit_channel_dialog = channel_editor.ChannelPropertyEditor(self.widget, parent=None)
        edit_channel_dialog.exec_()

    def actions(self):
        return [self.edit_channel_action]


class CernMemberSheetExtension(QtDesigner.QPyDesignerMemberSheetExtension):

    def __init__(self, widget: QtWidgets.QWidget, parent: QtCore.QObject):
        super(CernMemberSheetExtension, self).__init__(parent)
        self._widget = widget
        print(f'Loading CERN Member Sheet extension for object {repr(widget)}')

    def count(self) -> int:
        return 1

    def declaredInClass(self, index: int) -> str:
        return 'CernRandomButtonPlugin'

    def indexOf(self, name: str) -> int:
        return int(name == 'CernRandomButtonPlugin') - 1

    def inheritedFromWidget(self, index: int) -> bool:
        return False

    def isSignal(self, index: int) -> bool:
        return False

    def isSlot(self, index: int) -> bool:
        return True

    def isVisible(self, index: int) -> bool:
        return True

    def memberGroup(self, index: int) -> str:
        return 'My Custom Group'

    def memberName(self, index: int) -> str:
        return 'textFn' if index == 0 else None

    def parameterNames(self, index: int) -> bytes:
        return b'text'

    def parameterTypes(self, index: int) -> bytes:
        return b'QString'

    def setMemberGroup(self, index: int, group: str):
        pass

    def setVisible(self, index: int, b: bool):
        pass

    def signature(self, index: int) -> str:
        return 'textFn(QString)'

    def __len__(self) -> int:
        return self.count()


class CernTaskMenuExtension(qtplugin_extensions.PyDMExtension):
    """ Due to the way PyDM is built, we cannot create this extension as a regular Task Menu extension,
    but must subclass PyDM plugin extension. The reason is that CernRandomButton is subclassed from PyDMPushButton,
    which takes over task menu registration by PyDM before we get to register it for the CernRandomButton, thus
    ignoring our regular way. Qt Designer in fact picks the first returned extension, which happens to be from
    PyDM factory, so our factory never gets the chance to make one."""

    def __init__(self, widget: QtWidgets.QWidget, parent: QtCore.QObject = None):
        super(CernTaskMenuExtension, self).__init__(parent)
        action1 = QtWidgets.QAction(text="CERN Action 1", parent=widget)
        action2 = QtWidgets.QAction(text="CERN Action 2", parent=widget)
        action1.triggered.connect(self.action1_triggered)
        action2.triggered.connect(self.action2_triggered)
        self._actions = [action1, action2]
        self._widget = widget

    def actions(self) -> typing.List[QtWidgets.QAction]:
        return self._actions

    def action1_triggered(self, state: bool):
        del state
        print(f'Action 1 triggered! {self._widget}')

    def action2_triggered(self, state: bool):
        del state
        print(f'Action 2 triggered! {self._widget}')


class CernContainerExtension(QtDesigner.QPyDesignerContainerExtension):

    def __init__(self, widget: QtWidgets.QWidget, parent: QtCore.QObject):
        super(CernContainerExtension, self).__init__(parent)
        self._widget = widget
        self.__widgets = []
        self.__idx = -1
        self.setWidgetStyleForPage(0)

    def addWidget(self, widget: QtWidgets.QWidget):
        self.__widgets.append(widget)

    def count(self) -> int:
        return len(self.__widgets)

    def currentIndex(self) -> int:
        return self.__idx

    def insertWidget(self, index: int, widget: QtWidgets.QWidget):
        self.__widgets.insert(index, widget)

    def remove(self, index: int):
        del self.__widgets[index]

    def setCurrentIndex(self, index: int):
        self.__idx = index
        self.setWidgetStyleForPage(index)

    def widget(self, index: int) -> QtWidgets.QWidget:
        try:
            return self.__widgets[index]
        except IndexError:
            return None

    def setWidgetStyleForPage(self, index: int):
        page_group = (index % 7) + 1 # we support 3 bits, thus 0-7 range, but to exclude black (000),
                                     # we limit to 0-6 and shift by 1.
        bitmask = bin(page_group)[2:].zfill(3)  # creates a binary representation of index with 3 bits (000)

        def channel(idx: int) -> int:
            """ Creates a channel value from a bitmask """
            return int(bitmask[idx]) * 255

        css = f'background-color: rgb({channel(0)}, {channel(1)}, {channel(2)});'
        self._widget.setStyleSheet(css)


class CernFrameTaskMenuExtension(QtDesigner.QPyDesignerTaskMenuExtension):
    """ Because custom frame that is using this extension is not inheriting from PyDM widget,
        the logic for defining a custom extension is not limited and we can do it in a regular way here.
    """

    def __init__(self, widget: QtWidgets.QWidget, parent: QtCore.QObject = None):
        super(CernFrameTaskMenuExtension, self).__init__(parent)
        self.action = QtWidgets.QAction(text="Circulate pages", parent=widget)
        self._widget = widget
        self.action.triggered.connect(self.action_triggered)

    def preferredEditAction(self) -> QtWidgets.QAction:
        return self.action

    def taskActions(self) -> typing.List[QtWidgets.QAction]:
        return [self.action]

    def actions(self) -> typing.List[QtWidgets.QAction]:
        return self._actions

    def action_triggered(self, state: bool):
        del state
        container: CernContainerExtension = self._widget.container # We are sure, because we injected
                                                                   # this prop in CernFrameMultiPagePlugin
        count = container.count()
        if count > 0:
            container.setCurrentIndex((container.currentIndex() + 1) % container.count())


class CernPropertySheetExtension(QtDesigner.QPyDesignerPropertySheetExtension):

    property_to_ignore = 'protectedPassword'

    @introspect
    def __init__(self, widget: QtWidgets.QWidget, parent: QtCore.QObject):
        super(CernPropertySheetExtension, self).__init__(parent)
        self._widget = widget
        self._is_attribute : typing.List[bool] = []
        self._is_changed : typing.List[bool] = []
        self._is_visible : typing.List[bool] = []
        self._prop_group : typing.Dict[int, str] = {}
        print(f'Loading CERN Property Sheet extension for object {repr(widget)}')
        print(f'Ignored property at idx {self._ignored_idx()}')
        for i in range(self.count()):
            self._is_attribute.append(False)
            self._is_changed.append(False)
            self._is_visible.append(True)

    @introspect
    def count(self) -> int:
        cnt = self._widget.metaObject().propertyCount()
        # return cnt - 1 if self._ignored_idx() >= 0 else cnt
        return cnt

    @introspect
    def hasReset(self, index: int) -> bool:
        return self._widget.metaObject().property(index).isResettable()

    @introspect
    def indexOf(self, name: str) -> int:
        if name == 'CONCEALED':
            return self._ignored_idx()
        idx =  self._widget.metaObject().indexOfProperty(name)
        # if self._ignored_idx() >= 0 and idx >= self._ignored_idx():
        #     idx -= 1 # Shift by one, pretending ignored property is not in the list
        return idx
        # return self._widget.metaObject().indexOfProperty(name)

    @introspect
    def isAttribute(self, index: int) -> bool:
        return self._is_attribute[index]

    @introspect
    def isChanged(self, index: int) -> bool:
        return self._is_attribute[index]

    @introspect
    def isVisible(self, index: int) -> bool:
        return self._is_visible[index]

    @introspect
    def property(self, index: int) -> QtCore.QVariant:
        # idx = index + 1 if self._ignored_idx() >= 0 and index >= self._ignored_idx() else index
        idx = index
        prop = self._widget.metaObject().property(idx)

        print(f'Calling property at idx {idx} ({prop.name()}: {prop.typeName()})')
        val = prop.read(self._widget)
        return QtCore.QVariant(val)
        # if not val.isValid():
        #     return None
        # return val
        # try:
        #     return
        #     # return prop
        #
        #     # This will always return a string/num value instead of enums and other fancy things
        #     # res = prop.read(self._widget)
        #     # try:
        #     #     return res.toPyObject()
        #     # except AttributeError:
        #     #     return res
        # except TypeError:
        #     return None

    @introspect
    def propertyGroup(self, index: int) -> str:
        try:
            return self._prop_group[index]
        except KeyError:
            return 'I AM PYTHON!'

    @introspect
    def propertyName(self, index: int) -> str:
        if index == self._ignored_idx():
            return 'CONCEALED'
        return self._widget.metaObject().property(index).name()

    @introspect
    def reset(self, index: int) -> bool:
        return self._widget.metaObject().property(index).reset(self._widget)

    @introspect
    def setAttribute(self, index: int, b: bool):
        self._is_attribute[index] = b

    @introspect
    def setChanged(self, index: int, changed: bool):
        self._is_attribute[index] = changed

    @introspect
    def setProperty(self, index: int, value: typing.Any):
        name = self.propertyName(index)
        self._widget.setProperty(name, value)

    @introspect
    def setPropertyGroup(self, index: int, group: str):
        self._prop_group[index] = group

    @introspect
    def setVisible(self, index: int, b: bool):
        self._is_visible[index] = b

    def _ignored_idx(self) -> int:
        return self._widget.metaObject().indexOfProperty(self.property_to_ignore)


class CernExtensionFactory(QtDesigner.QExtensionFactory):

    def __init__(self, parent=None, custom_args: typing.Dict[str, typing.Any] = {}):
        super(CernExtensionFactory, self).__init__(parent)
        self._custom_args = custom_args

    def createExtension(self, obj: QtCore.QObject, iid: str, parent: QtCore.QObject):

        if isinstance(obj, cern_widgets.CernRandomButton):
            # if iid == _PROPERTY_SHEET_EXTENSION_IID:
            #     print(f'Creating a property sheet extension for obj {repr(obj)} and parent {repr(parent)}')
                # return CernPropertySheetExtension(widget=obj, parent=parent)
                # return CernCppCopyPropertySheetExtension(widget=obj, parent=parent)
                # return PyCernQt.QtDesigner.DemoPropertySheetExtension(obj, parent)
                # return PyCernQt.QtDesigner.JAPCPropertySheetExtension(obj, parent)
                # return CyQtDesigner.MyDesignerPropertySheetExtension()
                # return cycernqt.CernCythonPropertySheetExtension(obj, parent)
            if iid == _MEMBER_SHEET_EXTENSION_IID:
                print(f'Creating a member sheet extension for obj {repr(obj)} and parent {repr(parent)}')
                return CernMemberSheetExtension(widget=obj, parent=parent)
        elif isinstance(obj, cern_widgets.CernMultiPageFrame):
            if iid == _CONTAINER_EXTENSION_IID:
                print(f'Creating a container extension for obj {repr(obj)} and parent {repr(parent)}')
                return CernContainerExtension(widget=obj, parent=parent)
            elif iid == _TASK_MENU_EXTENSION_IID:
                print(f'Creating a task menu extension for obj {repr(obj)} and parent {repr(parent)}')
                return CernFrameTaskMenuExtension(widget=obj, parent=parent)
        # elif isinstance(obj, QtWidgets.QFrame):
        #     if iid == _PROPERTY_SHEET_EXTENSION_IID:
        #         print(f'Creating a property sheet extension for obj {repr(obj)} and parent {repr(parent)}')
        #         return CernTrickyPropertySheetExtension(widget=obj, parent=parent, **self._custom_args)


        return super(CernExtensionFactory, self).createExtension(obj, iid, parent)


# Push Button plugin
_CERN_WIDGET_GROUP = 'CERN widgets'
_CernRandomButtonPlugin = qtplugin_factory(cls=cern_widgets.CernRandomButton,
                                           group=_CERN_WIDGET_GROUP,
                                           # We have to pass a custom TaskMenu extension here
                                           # (see explanation at CernTaskMenuExtension docstring)
                                           extensions=[JapcSelectorExtension, CernTaskMenuExtension])

_CernMultiPageFramePlugin = qtplugin_factory(cls=cern_widgets.CernMultiPageFrame,
                                             group=_CERN_WIDGET_GROUP,
                                             is_container=True)
                                            # extensions=[CernFrameTaskMenuExtension])

_CernCustomEditorPlugin = qtplugin_factory(cls=cern_widgets.CernCustomSheetFrame,
                                           group=_CERN_WIDGET_GROUP)


class CernRandomButtonPlugin(_CernRandomButtonPlugin):

    def icon(self):
        return QtGui.QIcon.fromTheme('accessories-calculator')
        # return QtGui.QIcon(QtGui.QPixmap(_LOGO_XPM))

    def initialize(self, core):
        if self.initialized:
            print('Won\'t initialized. Already done.')
            return

        super(CernRandomButtonPlugin, self).initialize(core)
        self.initialized = False # Not yet (was assigned in super)

        # Manager is not instantiated in the base class if no extensions are provided in the factory call
        if not self.manager:
            self.manager = core.extensionManager()

        factory = CernExtensionFactory(parent=self.manager)

        self.manager.registerExtensions(factory, iid=_PROPERTY_SHEET_EXTENSION_IID) # This crashes the designer
        self.manager.registerExtensions(factory, iid=_MEMBER_SHEET_EXTENSION_IID)
        # This has no effect (see explanation at CernTaskMenuExtension docstring)
        self.manager.registerExtensions(factory, iid=_TASK_MENU_EXTENSION_IID)
        self.initialized = True


class CernMultiPageFramePlugin(_CernMultiPageFramePlugin):

    def initialize(self, core: QtDesigner.QDesignerFormEditorInterface):
        if self.initialized:
            print('Won\'t initialized. Already done.')
            return

        super(CernMultiPageFramePlugin, self).initialize(core)
        self.initialized = False # Not yet (was assigned in super)

        # Manager is not instantiated in the base class if no extensions are provided in the factory call
        if not self.manager:
            self.manager = core.extensionManager()

        factory = CernExtensionFactory(parent=self.manager)
        self.manager.registerExtensions(factory, iid=_CONTAINER_EXTENSION_IID)
        self.manager.registerExtensions(factory, iid=_TASK_MENU_EXTENSION_IID)
        self.initialized = True

    def icon(self):
        return QtGui.QIcon.fromTheme('applications-science')

    def createWidget(self, parent):
        """
        Because we do not inject extensions through PyDM factory, we need to find a way to link them, so that
        they can be accessed from the widget's instance.
        """
        w = super(CernMultiPageFramePlugin, self).createWidget(parent)
        setattr(w, "container", self.manager.extension(w, _CONTAINER_EXTENSION_IID))
        return w


timer = None


class CernCustomFormModificationPlugin(_CernCustomEditorPlugin):

    def __init__(self):
        super(CernCustomFormModificationPlugin, self).__init__()
        self._form_editor = None

    def icon(self):
        return QtGui.QIcon.fromTheme('applications-office')

    def initialize(self, core: QtDesigner.QDesignerFormEditorInterface):
        if self.initialized:
            print('Won\'t initialized. Already done.')
            return

        self._form_editor = core

        super(CernCustomFormModificationPlugin, self).initialize(core)
        self.initialized = False  # Not yet (was assigned in super)

        # Manager is not instantiated in the base class if no extensions are provided in the factory call
        if not self.manager:
            self.manager = core.extensionManager()

        # factory = CernExtensionFactory(parent=self.manager, custom_args={
        #     'core': core
        # })
        # self.manager.registerExtensions(factory, iid=_PROPERTY_SHEET_EXTENSION_IID)
        global timer

        def investigate_stuff():
            cnt = core.formWindowManager().formWindowCount()
            print(f'==== FORM WINDOW COUNT {cnt}')
            for i in range(cnt):
                print(f'=== FORM {i} = {core.formWindowManager().formWindow(i).fileName()}')

        timer = threading.Timer(10, investigate_stuff)
        timer.start()

        try:
            self._core.propertyEditor().setReadOnly(True)
            print('===== DEBUG SET READONLY')
        except AttributeError:
            print('====== PROP EDITOR DOES NOT EXIST')
            pass
        self.initialized = True


class CernCppCopyPropertySheetExtension(QtDesigner.QPyDesignerPropertySheetExtension):

    num_full_props = 1
    num_japc_components = 3
    num_timing_components = 3
    available_props: typing.List[str] = [
        'JAPC address',
        'JAPC device',
        'JAPC property',
        'JAPC property field',
        'Timing machine',
        'Timing group',
        'Timing user',
    ]

    def __init__(self, widget: QtWidgets.QWidget, parent: QtCore.QObject):
        super(CernCppCopyPropertySheetExtension, self).__init__(parent)
        self._widget = widget
        self._ignore_next_full_str_update: bool = False
        self._timing = []
        self._japc_addr = []

    def count(self) -> int:
        return self.num_full_props + self.num_japc_components + self.num_timing_components

    def hasReset(self, index: int) -> bool:
        _ = index
        return True

    def indexOf(self, name: str) -> int:
        try:
            return self.available_props.index(name)
        except ValueError:
            return -1

    def isAttribute(self, index: int) -> bool:
        return index >= self.num_full_props

    def isChanged(self, index: int) -> bool:
        if index == 0:
            val = self._resulting_addr()
            return val is not None and len(val) > 0
        elif index >= self.num_full_props:
            new_idx, list = self._split_prop_zone(index=index)
            if len(list) <= new_idx:
                return False
            component = list[new_idx]
            return component is not None and len(component) > 0
        return False

    def isVisible(self, index: int) -> bool:
        _ = index
        return True

    def property(self, index: int) -> QtCore.QVariant:
        if index == 0:
            val = self._resulting_addr()
            return QtCore.QVariant(val)
        elif index >= self.num_full_props:
            new_idx, list = self._split_prop_zone(index=index)
            if len(list) <= new_idx:
                return QtCore.QVariant('')
            return QtCore.QVariant(list[new_idx])
        return QtCore.QVariant()

    def propertyGroup(self, index: int) -> str:
        if index >= self.num_full_props + self.num_japc_components:
            return 'Timing Selector Override'
        elif index >= self.num_full_props and index < self.num_full_props + self.num_japc_components:
            return 'JAPC property-device model'
        else:
            return 'CERN'

    def propertyName(self, index: int) -> str:
        return self.available_props[index]

    def reset(self, index: int) -> bool:
        if index == 0:
            self._japc_addr.clear()
            self._timing.clear()
            return True
        else:
            def callback(new_idx: int, list: typing.List[str]):
                list[new_idx] = ''

            self._split_prop_zone(index=index, update=callback)
            return True
        return False

    def setAttribute(self, index: int, b: bool):
        pass

    def setChanged(self, index: int, changed: bool):
        pass

    def setProperty(self, index: int, value: QtCore.QVariant):
        if index == 0:
            if self._ignore_next_full_str_update:
                self._ignore_next_full_str_update = False
                return
            self._japc_addr.clear()
            self._timing.clear()
            str_val = str(value)
            regex = re.match(pattern='japc:\\/\\/([a-z0-9_-]+)(\\/([a-z0-9_-]+)(#([a-z0-9_-]+)(@([a-z0-9_-]+)(\\.([a-z0-9_-]+)(\\.([a-z0-9_-]+))?)?)?)?)?',
                             string=str_val,
                             flags=re.RegexFlag.IGNORECASE)
            count = len(regex.groups()) if regex else 0
            if count >= 1:
                self._japc_addr.append(regex.group(1))
                if count >= 3:
                    self._japc_addr.append(regex.group(3))
                    if count >= 5:
                        self._japc_addr.append(regex.group(5))
            if count >= 7:
                self._timing.append(regex.group(7))
                if count >= 9:
                    self._timing.append(regex.group(9))
                    if count >= 11:
                        self._timing.append(regex.group(11))
        else:
            str_val = str(value)

            def callback(new_idx: int, list: typing.List[str]):
                while len(list) <= new_idx:
                    list.append('')
                list[new_idx] = str_val

            self._split_prop_zone(index=index, update=callback)
            self._ignore_next_full_str_update = True

    def setPropertyGroup(self, index: int, group: str):
        pass

    def setVisible(self, index: int, b: bool):
        pass

    def _split_prop_zone(self, index: int, update: typing.Callable = None) -> typing.Tuple[int, typing.List[str]]:
        if index < self.num_full_props:
            raise RuntimeError('Given index is not intended for string lists')
        if index >= self.num_full_props and index < self.num_full_props + self.num_japc_components:
            new_idx = index - self.num_full_props
            list = self._japc_addr
        else:
            new_idx = index - self.num_full_props - self.num_japc_components
            list = self._timing
        if update:
            update(new_idx, list)
        return new_idx, list

    def _resulting_addr(self) -> str:
        japc_cnt = len(self._japc_addr)
        time_cnt = len(self._timing)
        if japc_cnt == 0 and time_cnt == 0:
            return ''

        result = 'japc://'
        if japc_cnt > 0:
            result += self._japc_addr[0]
            if japc_cnt > 1:
                val = self._japc_addr[1]
                if val:
                    result += '/' + val
                    if japc_cnt > 2:
                        val = self._japc_addr[2]
                        if val:
                            result += '#' + val
        if time_cnt > 0:
            val = self._timing[0]
            if val:
                result += '@' + val
                ran = min(time_cnt, 3)
                for i in range(1, ran):
                    val = self._timing[i]
                    if not val:
                        break
                    result += '.' + val
        return result


class CernTrickyPropertySheetExtension(QtDesigner.QPyDesignerPropertySheetExtension):

    def __init__(self, widget: QtWidgets.QWidget, parent: QtCore.QObject, core: QtDesigner.QDesignerFormEditorInterface, **kwargs):
        super(CernTrickyPropertySheetExtension, self).__init__(parent)
        self._widget = widget
        self._core = core
        # self.core.setPropertyEditor(CernPropertyEditor(parent=widget, core=core))

    def count(self) -> int:
        try:
            self._core.propertyEditor().setReadOnly(True)
            print('===== DEBUG SET READONLY')
        except AttributeError:
            pass
        return 0

    def hasReset(self, index: int) -> bool:
        return False

    def indexOf(self, name: str) -> int:
        return -1

    def isAttribute(self, index: int) -> bool:
        return False

    def isChanged(self, index: int) -> bool:
        return False

    def isVisible(self, index: int) -> bool:
        return True

    def property(self, index: int) -> typing.Any:
        return None

    def propertyGroup(self, index: int) -> str:
        return 'Only group'

    def propertyName(self, index: int) -> str:
        return None

    def reset(self, index: int) -> bool:
        return True

    def setAttribute(self, index: int, b: bool):
        pass

    def setChanged(self, index: int, changed: bool):
        pass

    def setProperty(self, name: str, value: typing.Any):
        pass

    def setPropertyGroup(self, index: int, group: str):
        pass

    def setVisible(self, index: int, b: bool):
        pass


class CernPropertyEditor(QtDesigner.QDesignerPropertyEditorInterface):

    def __init__(self, parent: QtWidgets.QWidget, core: QtDesigner.QDesignerFormEditorInterface, flags: QtCore.Qt.WindowFlags = QtCore.Qt.WindowFlags()):
        super(CernPropertyEditor, self).__init__(parent, flags)
        self._core = core

    def core(self) -> QtDesigner.QDesignerFormEditorInterface:
        return self._core

    def currentPropertyName(self) -> str:
        return ''

    def isReadOnly(self) -> bool:
        return True

    def object(self) -> QtCore.QObject:
        return None;

    def setObject(self, object: QtCore.QObject):
        pass

    def setProperty(self, name: str, value: typing.Any, changed: bool = True):
        pass

    def setReadOnly(self, readOnly: bool):
        pass