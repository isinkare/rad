from PyQt5 import QtGui, QtWidgets, QtCore

class SenderDisplay(QtWidgets.QWidget):

    text_edited = QtCore.pyqtSignal(str)

    def __init__(self, parent, *args, **kwargs):
        super(SenderDisplay, self).__init__(parent=parent, *args, **kwargs)

        # Reference to the PyDMApplication
        self.app = QtGui.QApplication.instance()
        # Assemble the Widgets
        self.line_edit = None
        self.setup_ui(parent)

    def setup_ui(self, parent):
        # Create the main layout
        main_layout = QtGui.QVBoxLayout(parent)
        self.setLayout(main_layout)
        self.line_edit = QtWidgets.QLineEdit()
        main_layout.addWidget(self.line_edit)
        self.line_edit.textEdited.connect(self._on_text_changed)

    @QtCore.pyqtSlot(str)
    def _on_text_changed(self, text: str):
        self.text_edited.emit(text + ' (decorated)')
