import typing
import os
from PyQt5 import QtWidgets, QtCore, uic


class TimingSelectorMenu(QtWidgets.QWidget):
    """ UI to select timing user override.

    It allows constructing selector strings, such as LHC.USER.ALL.
    """

    selector_changed = QtCore.pyqtSignal(str)
    """ Fired when one of the selector components is updated. """

    def __init__(self, parent: QtCore.QObject = None):
        super(TimingSelectorMenu, self).__init__(parent)
        self._user = None
        self._group = None
        self._machine = None
        self._setup_ui()

    @QtCore.pyqtSlot()
    def on_none_clicked(self):
        """ Slot to react to 'None' selector button. """
        self.selector = None

    def on_user_changed(self):
        """ Slot to react to User combobox change. """
        self._user = self.user_combobox.currentText()
        self.selector_changed.emit(self.selector)

    def on_group_changed(self):
        """ Slot to react to Group combobox change. """
        self._group = self.group_combobox.currentText()
        self.selector_changed.emit(self.selector)

    def on_machine_changed(self):
        """ Slot to react to Machine combobox change. """
        self._machine = self.machine_combobox.currentText()
        self.selector_changed.emit(self.selector)

    @QtCore.pyqtProperty(str)
    def selector(self) -> typing.Optional[str]:
        """ Full selector string representation, e.g. LHC.USER.ALL.

        Returns None, if one of the components is not available.
        """
        if not self._user or not self._group or not self._machine:
            return None
        return f'{self._machine}.{self._group}.{self._user}'

    @selector.setter
    def selector(self, new_value: typing.Optional[str]):
        """ Sets new selector and updates UI accordingly. """
        if not new_value:
            self.user_combobox.setCurrentText(None)
            self.group_combobox.setCurrentText(None)
            self.machine_combobox.setCurrentText(None)
        else:
            try:
                machine, group, user = new_value.split('.')
                self.user_combobox.setCurrentText(user)
                self.group_combobox.setCurrentText(group)
                self.machine_combobox.setCurrentText(machine)
            except ValueError:
                raise TypeError('Expected timing selector format is "USER.GROUP.MACHINE"')

    def _setup_ui(self):
        """ Loads UI file and binds signals/slots. """
        uic.loadUi(os.path.join(os.path.dirname(os.path.realpath(__file__)), 'timing.ui'), self)
        self.none_button.clicked.connect(self.on_none_clicked)
        self.user_combobox.currentTextChanged.connect(self.on_user_changed)
        self.group_combobox.currentTextChanged.connect(self.on_group_changed)
        self.machine_combobox.currentTextChanged.connect(self.on_machine_changed)
